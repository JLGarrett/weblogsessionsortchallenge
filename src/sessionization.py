#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is the main code used for to read the weblogs of the Edgar
database for the Insight coding challenge.

The bash script input should be log.csv, inactivity_period.txt,
sessionization.txt

It requires both the sys and csv libraries.

Created on Wed May 23 23:46:54 2018

@author: Joe Garrett
"""

import logRead as lr
import sys

#read the time limit from ./input/inactivity_period.txt
timeLimit = lr.getIdleTime(sys.argv[2])

# run the main code
lr.CSVreaderEDGAR( sys.argv[1], sys.argv[3], timeLimit )

print('Weblog session analysis complete.')