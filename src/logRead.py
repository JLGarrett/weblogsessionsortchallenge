# -*- coding: utf-8 -*-
"""
Several functions and classes designed to read the weblog's of the IP addresses
used to dowload documents from the SEC's EDGAR database. 

The functions were designed to complete the coding challenge portion of the 
application for the Insight Data engineering program.

These functions and classes are used by mainInsight.py to convert the list of 
individual document downloads in log.csv to a list of ip addresses containing
the duration of the their time on EDGAR and the number of documents downloaded.

This file relies on the csv Python library and was written with the Spyder 
Editor.

@author: Joe Garrett
"""

import csv

def getIdleTime( inactiveFile ):
    '''Reads the file inactivity_period.txt in the input directory to determine
    the inactivity period used to determine when a session has closed'''
    with open(inactiveFile) as f:
        read_data = f.read()
    return int(read_data)

class Session:
    '''Keeps track of document download number and time for an ip address'''
    def __init__(self, ipAddress, date, startTime):
        self.ipAddress = ipAddress
        self.date = date
        self.startTime = startTime
        self.lastTime = startTime
        self.idleTime = 0 
        self.docNumber = 1
        return None
        
    def addDoc(self, addTime):
        '''The ip address downloads a document at time addTime'''
        self.lastTime = addTime
        self.idleTime = 0
        self.docNumber += 1
        #If a record of the details of the document were needed, that could
        #easily be added here, but for the purposes of this initial test,
        #it would only slow it down
        return 0
        
    def incrementTime(self, duration):
        '''Increases the idle time of a session, to determine when it ends'''
        self.idleTime += duration
        return 0
    
    def closeSession(self):
        '''Deletes a session and returns its properties'''
        output = {'ip':self.ipAddress, 'date':self.date, 
                  'start':self.startTime, 'end':self.lastTime, 
                  'docs': self.docNumber}
        #Once a session is done, we go ahead and close it to save memory
        del self
        return output
    
def csListToCSVoutput( sessionDict ):
    '''Converts the closeSession dictionary to a form to be stored in .txt'''
    csvOutput = [sessionDict['ip'], sessionDict['date'] + ' ' + \
               sessionDict['start'], sessionDict['date'] + ' ' + \
               sessionDict['end'], quickTime(sessionDict['end']) - \
               quickTime(sessionDict['start']) +1, sessionDict['docs']]
    return csvOutput
    
def CSVreaderEDGAR( CSVin, CSVout, timeLimit ):
    '''Reads EDGAR-style data in from the file CSVname, sorts the data into 
    sessions based on the timeLimit, and outputs the data to 
    output/sessionization.txt. '''
    #we begin by opening both the file we are reading from and the file we
    #are outputting because that is most similar to the data stream described
    #in Insight's prompt
    with open(CSVin, 'r') as csvfile:
       with open(CSVout, 'w', newline='') as f:
           #We use the csv library because it is a clear and simple way to 
           #sort through .csv files with headers
           CSVreader = csv.DictReader(csvfile, delimiter=',', quotechar='|')
           edgarwriter = csv.writer(f)
           
           #The first row is used to inialize the list of open sessions
           initializingRow = next(CSVreader)
           openSessions = sessionsRecord(initializingRow['date'], timeLimit)
           openSessions.newDoc(initializingRow['ip'], 
                           initializingRow['time'])

           #The rest of the csv is read row-by-row and the session record is 
           #output as the data are read  
           for row in CSVreader:
               try:
                   date = row['date']
                   
                   #We check to make sure that the date is the same for the
                   #session and the input row
                   if date == openSessions.date:
                       
                       #The document accessed in the row is added to the 
                       #record of open sessions
                       openSessions.newDoc(row['ip'], row['time'])
                       
                       #after each row, the expired sessions are written
                       #to file and removed from the record
                       poppedSession = openSessions.popExpiredSession()
                       stepDictList = []
                       while poppedSession is not None:
                           popDict = poppedSession.closeSession()

                           #instead of writing directly to disk, we first 
                           #create a list with all the expired sessions  
                           stepDictList.append(popDict)
                           poppedSession = openSessions.popExpiredSession()
                           
                   else:
                       #if the date has changed, all the sessions are closed
                       #and saved to disk
                   
                       print('New date detected -- Outputing current data')                    
                   
                       poppedSession = openSessions.popNextSession()

                       while poppedSession is not None:
                           popDict = poppedSession.closeSession()
                           stepDictList.append(popDict)
                           poppedSession = openSessions.popNextSession()
                       
                   orderedDictList = quickDictListSort(stepDictList)
                   
                    #finally, all the expired sessions are written to disk
                   for i in stepDictList:    
                       poplist = csListToCSVoutput(i)
                       edgarwriter.writerow(poplist)    
                       
               except KeyError:
                   print('Error! No date! Skipping row!')
           
           try:     
               poppedSession = openSessions.popNextSession()
           except NameError:
               #This just covers the case when the date has changed
               return None
               
           endDictList = []
           while poppedSession is not None:
                popDict = poppedSession.closeSession()
                endDictList.append(popDict)
                poppedSession = openSessions.popNextSession()
                
           orderedDictList = quickDictListSort(endDictList)
           
           for i in orderedDictList:     
                popList = csListToCSVoutput(i)
                edgarwriter.writerow(popList)
                

def quickTime( timeStr ):
    '''Quickly converts the time from the form of HH:MM:SS to seconds'''
    return sum([int(i)*t for i,t in zip(timeStr.split(':'),[3600,60,1])])

def quickDictListSort( dictList ):
    '''Sorts a dictionary based on the quantity 'start'''
    return sorted(dictList, key=lambda x: quickTime(x['start']))

class sessionsRecord:
    '''A list of sessions some extra metadata and functions included'''
    def __init__(self, date, timeLimit):
        self.sessions = []
        self.openIPs = []
        self.date = date
        self.expiredSessions = []
        self.timeLimit = timeLimit + 1 # the +1 makes it inclusive
        
    def appendSession(self, session):
        '''Adds a session to the list of sessions'''
        self.sessions.append(session)
        self.openIPs.append(session.ipAddress)
        
    def removeSession(self, ipAddress):
        '''Removes a session from the list of sessions'''
        ipLoc = self.ipOpenLoc(ipAddress)
        if(self.isIPOpen(ipAddress) > -1):
            del self.openIPs[ipLoc]
            if(ipAddress == self.sessions[ipLoc].ipAddress):
                del self.sessions[ipLoc]
        
    def ipOpenLoc(self, ipAddress):
        '''Determines whether or not a session with ipAddress is in the list
        of sessions'''
        try:
            return self.openIPs.index(ipAddress)
        except ValueError:
            return -1
    
    def newDoc(self, ipAddress, addTime):
        '''Adds a new document for a session with ipAddress at time addTime.'''
        try:
            lastTime = self.sessions[-1].lastTime
            duration = quickTime(addTime) - quickTime(lastTime)
            for i in self.sessions:
                i.incrementTime(duration)
        except IndexError:
            duration = 0
        
        ipLoc = self.ipOpenLoc(ipAddress)
        if(ipLoc > -1):
            self.sessions[ipLoc].addDoc(addTime)
            self.openIPs.append(self.openIPs.pop(ipLoc))
            self.sessions.append(self.sessions.pop(ipLoc))
        else:
            self.appendSession(Session(ipAddress, self.date, addTime))
    
    def popExpiredSession(self):
        '''Pops the next expired session from the list. If no sessions are 
        expired, then returns None.'''
        i = self.sessions[0]
        if i.idleTime < self.timeLimit:
            return None # no ExpiredSessions to pop
        else:
            self.openIPs.pop(0)
            return self.sessions.pop(0)
        
    def popNextSession(self):
        '''Pops the next session whether or not it is expired. If not sessions 
        are open, the list is deleted and None is returned.'''
        if not self.openIPs:
            del self
            return None
        
        self.openIPs.pop(0)
        return self.sessions.pop(0)
    