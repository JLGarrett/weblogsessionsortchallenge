The code that I've written for this challenge is split into two files: logRead.py and sessionization.py.

The first, logRead.py is a collection of functions and classes that I coded to use read .csv files in the form of the EDGAR weblogs and output files according to the Insight challenge. 

The second, sessionization.py, is a script written to run the functions of logRead.py in a straightforward way using a Bash script.

My code depends on the csv and sys libraries, which I understand to be quite common. The library csv is used to facilitate reading the data, and sys is used to interface better with bash.

The code has been tested on some of the SEC's EDGAR weblogs, and I've included a 'test_2' that verifies the code on the first hour of data from 10/07/2011.

To run the code, simply run 'run.sh'.

-Joe Garrett
5/28/18

